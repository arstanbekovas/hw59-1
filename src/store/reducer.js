import {SAVE_TODO, DELETE, FETCH_TODO_SUCCESS, CHANGE_NAME} from "./actions";

const initialState = {
    todo: {},
    inputName: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SAVE_TODO:
            return {...state, todo: action.todo};
        case CHANGE_NAME:
            return {...state, inputName: action.name};
        // case DELETE:
        //     return  {...state, inputName:};
        case FETCH_TODO_SUCCESS:
            return {...state, todo: action.todo};
        // case CHANGE_NAME:
        //     return{inputName: action.inputName};
        default:
            return state;
    }
};



export default reducer;
