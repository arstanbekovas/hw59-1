import axios from '../axios-todo';


export const FETCH_TODO_REQUEST = 'FETCH_TODO_REQUEST';
export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR';
export const SAVE_TODO = 'SAVE_TODO';
export const DELETE = 'DELETE';
export const CHANGE_NAME = 'CHANGE_NAME';


export const saveTodo = () => {
    return (dispatch, getState) => {
        const inputName = getState().inputName;
        axios.post('/todo.json', {inputName}).then(response => {
            dispatch(fetchTodoSuccess(response.data));
        });
    }
};

export const deleteTodo = (id) => {
    return (dispatch, getState) => {
        const inputName = getState().inputName;
        axios.delete(`/todo/${id}.json`, inputName).then(response => {
            dispatch(fetchTodoSuccess(response.data));
        });
    }
};

export const save = todo => {
    return (dispatch) => {
        dispatch({ type: SAVE_TODO, todo});
        dispatch(saveTodo());
    };
};
export const deleteItem = (id) => {
    return (dispatch) => {
        dispatch({ type: DELETE });
        dispatch(deleteTodo(id));
    };
};

export const changeName = (e) => {
    return { type: CHANGE_NAME, name: e.target.value };
};

export const fetchTodoRequest = () => {
    return { type: FETCH_TODO_REQUEST };
};

export const fetchTodoSuccess = (todo) => {
    return { type: FETCH_TODO_SUCCESS, todo};
};

export const fetchTodoError = () => {
    return { type: FETCH_TODO_ERROR };
};


export const fetchTodo = () => {
    return dispatch => {
        dispatch(fetchTodoRequest());
        axios.get('/todo.json').then(response => {
            console.log(response.data);
            dispatch(fetchTodoSuccess(response.data));
        }, error => {
            dispatch(fetchTodoError());
        });
    }
};

