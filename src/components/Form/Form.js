import React, {Fragment} from 'react';
import './Form.css'

const Form = props => {
    return (
        <Fragment>
            <input className="Name" type="text" value={props.inputName}
                   placeholder="Movie name" onChange={props.changeName}/>
            <button className="Add" onClick={props.add}>Add</button>
        </Fragment>
    )
};

export default Form;