import React, {Component} from 'react';
import './Movie.css'

class Movie extends Component {
    shouldComponentUpdate (nextProps) {
        return nextProps.name !== this.props.name;
    }

    render () {
        return (
            <div className="Movie">
                <p className="Name">{this.props.name}</p>
                <button className="Remove" onClick={this.props.remove}>X</button>

            </div>
        );
    }

};

export default Movie;