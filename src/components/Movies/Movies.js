import React from 'react';
import Movie from "./Move/Movie";
import {connect} from "react-redux";

const Movies = props => {
    return (
        <div className="Movies">
            <h1>To Do list: </h1>
            {Object.keys(props.todo).map(key => {
                return <Movie
                    key={key}
                    name={props.todo[key].inputName}
                    remove={() => props.remove(key) }
                />

            })}
        </div>
    )
};


const mapStateToProps = state => {
    return {
        todo: state.todo

    }
};

export default connect(mapStateToProps, null)(Movies);