import React, { Component } from 'react';
import './App.css';
import Form from "./components/Form/Form";
import Movies from "./components/Movies/Movies";
import {connect} from "react-redux";
import { deleteItem, save, changeName, fetchTodo} from "./store/actions";


class App extends Component {

componentDidMount() {
    this.props.fetchTodo();
}

    render() {
        return (
            <div className="App">
                <Form
                    changeName={this.props.changeName}
                    add={this.props.save}
                />
                <Movies
                    // todo={this.state.todo}
                    // change={this.props.changeName}
                    // remove={this.props.deleteItem}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        inputName: state.inputName,
        todo: state.todo

    }
};
const mapDispatchToProps = dispatch => {
    return {
        changeName: (e) => dispatch(changeName(e)),
        deleteItem: (id) => dispatch(deleteItem(id)),
        save: () => dispatch(save()),
        fetchTodo: () => dispatch(fetchTodo())

    }
};
export default connect(mapStateToProps, mapDispatchToProps)(App);

